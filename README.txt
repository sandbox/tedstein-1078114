
This install profile creates a feature rich website.

***********************************************************
Installation instructions
***********************************************************

- Place the innerfile install profile in /profiles.

  /profiles/innerfile/innerfile.install
  /profiles/innerfile/README.txt

- Run the install script and choose the Inner File profile.
